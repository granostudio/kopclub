$('.access-block .btn-login').click(function(){
    $('.modal-login').toggleClass('login-off');
});
$('.modal-login .close-link').click(function(){
    $('.modal-login').toggleClass('login-off');
});
var btnModalLogin = $('.modal-login .modal-login-btn')
$('.modal-login .login').click(function(){
    event.preventDefault();
    if(btnModalLogin.hasClass('login')) {
        $('.modal-login .input-modal:nth-child(2)').removeClass('input-hidden');
        btnModalLogin.addClass('login-pt2');
        btnModalLogin.removeClass('login');
        $('.modal-login .remember-text-link').show();
        $('.modal-login .modal-login-desc .modal-login-desc-text').html('Complete com sua <span class="modal-login-desc-span">senha</span> para continuar.')
    }
    $('.modal-login .login-pt2').click(function(){ 
        event.preventDefault();
    if(btnModalLogin.hasClass('login-pt2')) {
        $('.modal-login .input-modal:nth-child(3)').removeClass('input-hidden');
        btnModalLogin.addClass('login-pt3');
        btnModalLogin.removeClass('login-pt2');
        $('.modal-login .remember-text-link').hide();
        $('.modal-login .modal-login-desc .modal-login-desc-text').html('Você ainda não tem uma senha cadastrada. Complete o campo abaixo para cadastrar a senha e acessar sua conta <span class="modal-login-desc-span">Kop Club</span>.');
        $('.modal-login .block-form-content .modal-login-btn').html('Cadastrar');
    }
    });
});