let btnRegistrado = $('#contactUsBtnRegistered'),
    btnNotRegistrado = $('#contactUsBtnNotRegistered'),
    step1 = $('#step1'),
    step2 = $('#step2');

btnRegistrado.click(function(){
    event.preventDefault();
    step1.hide();
    step2.show();
});
btnNotRegistrado.click(function(){
    event.preventDefault();
    step2.hide();
    step1.show();
});

