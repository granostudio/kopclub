let boxScore = $('.box-score');
$('.point-button').click(function(){
    boxScore.toggleClass("box-score-inativo");
    boxScore.toggleClass("box-score-ativo");
});

// FUNCAO DE MENU LOGADO ABRINDO
let menuBtn = document.querySelector('.menu-button');
let navMenu = document.getElementById("navMenu");
let logoImg = document.querySelector('.block-kopclub .logo-img');
let body = document.querySelector('body');

function openNav() {
    document.getElementById("navMenu").style.width = "100%";
    menuBtn.classList.add('active-menu');
    navMenu.classList.add('nav-menu-active');
    logoImg.classList.add('logo-ativo');
    let navMenuActive = document.querySelector('.active-menu');
    body.style.overflow = "hidden";
    
    navMenuActive.addEventListener('click', function(){
        closeNav();
    })
}

function closeNav() {
    document.getElementById("navMenu").style.width = "0";
    menuBtn.classList.remove('active-menu');
    navMenu.classList.remove('nav-menu-active');
    logoImg.classList.remove('logo-ativo');
    body.style.overflow = "auto";

    menuBtn.addEventListener('click', function(){
        openNav();
    })
}