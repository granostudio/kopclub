let darkBright = document.querySelector('.dark-bright img')
darkBright.ondragstart = function() { return false; };

let AppPhone = document.querySelector('.app-phone img')
AppPhone.ondragstart = function() { return false; };

// FUNCAO REMOVE SCROL AO ABRIR MODAL
// if ($('#modalKopRegisterFill').hasClass('show')) {
//     $('body').css('overflow','none');
// }

// FIM FUNCAO

// SLIDER TOPO
$('.kop-club-slide').slick({
    infinite: false,
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

// SLIDER PRODUTOS
// $('.product-slider').slick({
//     infinite: false,
//     arrows: true,
//     dots: false,
//     autoplay: true,
//     autoplaySpeed: 10000,
//     pauseOnHover: true,
//     slidesToShow: 3,
//     prevArrow: '<div class="slick-prev"><div class="prev-circle"><div class="prev-icon"></div></div></div>',
//     nextArrow: '<div class="slick-next"><a href="javascript:void(0)"><div class="next-circle"><div class="next-icon"></div></div></a></div>'
// });

$('.product-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: false,
    prevArrow: '<div class="slick-prev"><div class="prev-circle icon-circle"></div><div class="prev-icon icon-arrow"></div></div>',
    nextArrow: '<div class="slick-next"><div class="next-circle icon-circle"></div><div class="next-icon icon-arrow"></div></div>',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        }, 
        {
            breakpoint: 650,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            }
        }
    ]
});


// EFEITO DE ANIMACAO DOS BOTOES DO SLIDE DE PRODUTOS
// $.attractHover('.slick-prev .product-slider');
$.attractHover('.product-slider .slick-prev ',{
    attractClass: 'attract-hover',
    attractEasingClass: 'attract-hover-easing',
    attractAttractedClass: 'attract-hover-attracted',
    proximity: 30,
    magnetism: 3

});

$.attractHover('.product-slider .slick-next ',{
    attractClass: 'attract-hover',
    attractEasingClass: 'attract-hover-easing',
    attractAttractedClass: 'attract-hover-attracted',
    proximity: 30,
    magnetism: 3

});

// efeito menu
$.attractHover('.block-kopclub .menu-button',{
    attractClass: 'attract-hover',
    attractEasingClass: 'attract-hover-easing',
    attractAttractedClass: 'attract-hover-attracted',
    proximity: 40,
    magnetism: 3

});
// fim efeito menu


var cursorCircle = $(".your-cursor1");  
$(".product-slider .slick-prev").on("mouseenter", function() {
	cursorCircle.addClass("active");
});
$(".product-slider .slick-prev").on("mouseleave", function() {
	cursorCircle.removeClass("active");
});

    
$(".product-slider .slick-next").on("mouseenter", function() {
	cursorCircle.addClass("active");
});
$(".product-slider .slick-next").on("mouseleave", function() {
	cursorCircle.removeClass("active");
});

$(".block-kopclub .menu-button").on("mouseenter", function() {
	cursorCircle.addClass("active");
});
$(".block-kopclub .menu-button").on("mouseleave", function() {
	cursorCircle.removeClass("active");
});
    
// REMOCAO DE ROLAGEM BODY
let modal = $(".modal");

if (modal.hasClass("show")) {
    $(body).css('overflow','hidden');
}